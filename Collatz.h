// ------------------------------
// projects/c++/collatz/Collatz.h
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param s a string
 * @return a pair of ints, representing the beginning and end of a range, [i, j]
 */
pair<int, int> collatz_read (const string& s);

// ------------
// collatz_eval
// ------------

/**
 * @param i the beginning of the range, inclusive, 0 < i, j < 1,000,000
 * @param j the end of the range, inclusive
 * @return the max cycle length of the range [i, j], 0 < max < 1,000
 */
int collatz_eval (int i, int j);

// ----------------
// collatz_find_max
// ----------------

/**
 * @param current_length is cycle length of a number
 * @param max cycle length is max cycle length of a range
 * @return the max number between the two numbers
 */
int collatz_find_max (int current_length, int max_cycle_length);

// ------------------------------
// collatz_calculate_cycle_length
// ------------------------------

/**
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @return the max cycle length of the range [i, j]
 */
int collatz_calculate_cycle_length (uint64_t i, uint64_t j);


// --------------------------
// collatz_count_cycle_length
// --------------------------

/**
 * @param pos the number to go through collatz conjecture
 * @return the  cycle length of the pos
 */
int collatz_count_cycle_length (uint64_t pos);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param w an ostream
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @param v the max cycle length
 */
void collatz_print (ostream& w, int i, int j, int v);

// -------------
// collatz_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 */
void collatz_solve (istream& r, ostream& w);

#endif // Collatz_h
