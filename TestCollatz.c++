// ------------------------------------
// projects/c++/collatz/TestCollatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------


// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

//Adding own evalution tests
TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(1, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(1, 2);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(1, 1001);
    ASSERT_EQ(v, 179);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(1, 1002);
    ASSERT_EQ(v, 179);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(1, 4001);
    ASSERT_EQ(v, 238);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(99998, 99999);
    ASSERT_EQ(v, 227);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(2, 8);
    ASSERT_EQ(v, 17);
}

TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(50,50000);
    ASSERT_EQ(v, 324);
}

TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(3,8);
    ASSERT_EQ(v, 17);
}

TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(999998, 999999);
    const int b = collatz_eval(999998, 999999);
    ASSERT_EQ(v, b);
}

TEST(CollatzFixture, eval_15) {
    const int v = collatz_eval(1, 8);
    const int b = collatz_eval(8, 1);
    ASSERT_EQ(v, b);
}

TEST(CollatzFixture, eval_16) {
    const int v = collatz_eval(1,999999);
    ASSERT_EQ(v, 525);
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n1 1\n1 2 2\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1 1 1\n1 2 2\n", w.str());
}


// -------------------------------
// collatz_find_max- Helper Method
// -------------------------------

//Tests for checking and returning the max number given two inputs for check_max_cycle_length ()
TEST(CollatzFixture, max_1) {
    const int v = collatz_find_max(1,1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, max_2) {
    const int v = collatz_find_max(1,2);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, max_3) {
    const int v = collatz_find_max(2,1);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, max_4) {
    const int v = collatz_find_max(999999,1);
    ASSERT_EQ(v, 999999);
}


// -----------------------------------------
// collatz_count_cycle_length- Helper Method
// -----------------------------------------

//Tests if method returns correct cycle length
TEST(CollatzFixture, collatz_count_cycle_length_1) {
    const int v = collatz_count_cycle_length(1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, collatz_count_cycle_length_2) {
    const int v = collatz_count_cycle_length(2);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, collatz_count_cycle_length_3) {
    const int v = collatz_count_cycle_length(3);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, collatz_count_cycle_length_4) {
    const int v = collatz_count_cycle_length(100);
    ASSERT_EQ(v, 26);
}


// ---------------------------------------------
// collatz_calculate_cycle_length- Helper Method
// ---------------------------------------------

//Tests if method returns correct cycle length
TEST(CollatzFixture, collatz_calculate_cycle_length_1) {
    const int v = collatz_calculate_cycle_length(1, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, collatz_calculate_cycle_length_2) {
    const int v = collatz_calculate_cycle_length(1, 2);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, collatz_calculate_cycle_length_3) {
    const int v = collatz_calculate_cycle_length(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, collatz_calculate_cycle_length_4) {
    const int v = collatz_calculate_cycle_length(201, 210);
    ASSERT_EQ(v, 89);
}















