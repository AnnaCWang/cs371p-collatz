FROM gcc

RUN apt-get update                      && \
    apt-get -y install astyle           && \
    apt-get -y install cppcheck         && \
    apt-get -y install cmake            && \
    apt-get -y install doxygen          && \
    apt-get -y install libboost-all-dev && \
    apt-get -y install libgtest-dev     && \
    apt-get -y install valgrind         && \
    apt-get -y install vim              && \
    cd /usr/src/gtest                   && \
    cmake CMakeLists.txt                && \
    make                                && \
    cp *.a /usr/lib                     && \
    cd -

CMD bash
